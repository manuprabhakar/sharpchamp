(function () {
  'use strict';

  // Events controller
  angular
    .module('events')
    .controller('EventsController', EventsController);

  EventsController.$inject = ['$scope', '$state', '$window', 'Authentication', 'eventResolve','FileUploader','$timeout'];

  function EventsController ($scope, $state, $window, Authentication,event,FileUploader,$timeout) {
    var vm = this;
    $scope.user = Authentication.user;
    vm.authentication = Authentication;
    vm.event = event;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    $scope.m=false;$scope.f=false;
    // vm.event.eventImageURL="modules/events/client/img/profile/default.png";
    $scope.sport={name:'',first:'',second:'',third:''};
    $scope.objects=[];
    $scope.uploader = new FileUploader({
      url: 'api/event/picture'
    });
    $scope.male=function(){

        console.log($scope.m);
        console.log($scope.f);
        if($scope.m && $scope.f){
          vm.event.gender='both';
          $scope.m=false;$scope.f=false;
        }
          else if ($scope.f) {
            vm.event.gender='f';
            $scope.m=false;$scope.f=false;
          }
          else if ($scope.m) {
            vm.event.gender='m';
            $scope.m=false;$scope.f=false;
          }

        console.log(vm.event.gender);      }


        $scope.pushSport=function(){
          console.log($scope.sport);
          $scope.objects.push($scope.sport);
          vm.event.sports=$scope.objects;
          console.log($scope.objects);
          console.log(vm.event.sports);
          $scope.sport={name:'',first:'',second:'',third:''};

        };


        $scope.imageURL = vm.event._id?vm.event.eventImageURL:"modules/events/client/img/profile/default.png";

        $scope.removeFromArray = function(x) {
          console.log(x);
           var index = vm.event.sports.indexOf(x.sports);
          vm.event.sports.splice(index,1);
        };
    // Remove existing Event
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.event.$remove($state.go('events.list'));
      }
    }

    // Save Event
    function save(isValid) {
      vm.event.sports=$scope.objects;
      console.log(vm.event);

      // TODO: move create/update logic to service
      if (vm.event._id) {
        vm.event.$update(successCallback, errorCallback);}
      else {console.log('here');
       vm.event.$save(successCallback, errorCallback);
      }
      //
      function successCallback(res) {
        $state.go('events.view', {
          eventId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }

    $scope.uploader.filters.push({
      name: 'imageFilter',
      fn: function (item, options) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
      }
    });

    // Called after the user selected a new picture file
    $scope.uploader.onAfterAddingFile = function (fileItem) {
      if ($window.FileReader) {
        var fileReader = new FileReader();
        fileReader.readAsDataURL(fileItem._file);

        fileReader.onload = function (fileReaderEvent) {
          $timeout(function () {
            $scope.imageURL = fileReaderEvent.target.result;
          }, 0);
        };
      }
    };

    // Called after the user has successfully uploaded a new picture
    $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
      // Show success message
      $scope.success = true;

      vm.event.eventImageURL = response.path;

      // // Populate user object
      // $scope.user = Authentication.user = response;

      // Clear upload buttons
      $scope.cancelUpload();
    };

    // Called after the user has failed to uploaded a new picture
    $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
      // Clear upload buttons
      $scope.cancelUpload();

      // Show error message
      $scope.error = response.message;
      console.log($scope.error);
    };

    // Change user profile picture
    $scope.uploadProfilePicture = function () {
      // Clear messages
      $scope.success = $scope.error = null;

      // Start upload
      $scope.uploader.uploadAll();
    };

    // Cancel the upload process
    $scope.cancelUpload = function () {
      $scope.uploader.clearQueue();
      $scope.imageURL = vm.event.eventImageURL;
    };


  }
}());

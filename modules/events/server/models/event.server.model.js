'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var sportSchema=new Schema({
  name:String,
  first:String,
  second:String,
  third:String
});



/**
 * Event Schema
 */

var EventSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill Event name',
    trim: true
  },
  address: {
    type: String,
    default: '',
    trim: true
  },
  organizedBy: {
    type: String,
    default: '',
    trim: true
  },
  sports: {
    type: [sportSchema],
    default: ''
  },
  gender: {
    type: String, // M /F / Both
    default: 'both'
  },
  fees:{
    type: Number,
    default: 0
  },
  eventImageURL: {
    type: String,
    default: 'modules/events/client/img/profile/default.png' //yahan sw ni ayegi value bhai frontend pe bhi karna hoga o
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  created:{
    type:Date,
    default:Date.now
  }
});

mongoose.model('Event', EventSchema);
